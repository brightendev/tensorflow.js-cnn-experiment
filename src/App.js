import React from 'react';
import './App.css';

import {DatasetGenerator as Dataset} from './tensorflow/DatasetGenerator';
import {DigitDetector as Detector} from './tensorflow/DigitDetector';
import datasetSprite from './tensorflow/generated-dataset-2000.png';

class App extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return(
			<div>
				<button onClick={this.getDataset}>get dataset</button>
				<canvas id='canvas'></canvas>
				<button onClick={this.trainModel}>train model</button>
				<img src={datasetSprite} id='datasetSprite'></img>
				<canvas id='test'></canvas>
			</div>
		);
	}

	run = async() => {
		Dataset.load();
		
	}

	getDataset = async() => {
		const num = 2000;
		const images = await Dataset.getDatasets(num);

		const canvas = document.getElementById('canvas');
		
		const cols = 50;
		const rows = images.length / cols;

		canvas.width = 28*cols;
		canvas.height = 28*rows;
		const ctx = canvas.getContext('2d');
		console.log(images.length)
		let i = 0;
		for(let row = 0; row < rows; row++) {
			for(let col = 0; col < cols; col++) {
				ctx.drawImage(images[i], 0, 0, 28, 28, col*28, row*28, 28, 28);
				i++;
			}
		}
	}

	trainModel = async() => {
		const datasetSprite = document.getElementById('datasetSprite');
		const canvas = document.createElement('canvas');
		canvas.width = datasetSprite.width;
		canvas.height = datasetSprite.height;
		canvas.getContext("2d").drawImage(datasetSprite, 0, 0);

		// const test = document.getElementById('test');
		// test.width = canvas.width;
		// test.height = canvas.height;
		// test.getContext("2d").drawImage(canvas, 0, 0);

		// console.log(datasetSprite.width)
		Detector.train(canvas);
	}
}

export default App;
