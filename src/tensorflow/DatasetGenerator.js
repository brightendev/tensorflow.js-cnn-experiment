import * as tf from '@tensorflow/tfjs';
import {MnistDataset} from './MnistDataset';

export class DatasetGenerator {

    // constructor() {
    //     this.run();
    // }
    
    
    static load = async () => {
        const data = new MnistDataset();
        await data.load();
        await this.showExamples(data);
    }

    static showExamples = async (data) => {
        // Create a container in the visor
        // const surface = tfvis.visor().surface({ name: 'Input Data Examples', tab: 'Input Data' });

        // Get the examples
        const examples = data.nextTestBatch(20);
        const numExamples = examples.xs.shape[0];

        // Create a canvas element to render each example
        for (let i = 0; i < numExamples; i++) {
            const imageTensor = tf.tidy(() => {
                // Reshape the image to 28x28 px
                return examples.xs
                    .slice([i, 0], [1, examples.xs.shape[1]])
                    .reshape([28, 28, 1]);
            });

            const canvas = document.createElement('canvas');
            canvas.width = 28;
            canvas.height = 28;
            canvas.style = 'margin: 4px;';
            await tf.browser.toPixels(imageTensor, canvas);
            // surface.drawArea.appendChild(canvas);

            imageTensor.dispose();
        }
    }

    static getDatasets = async (number) => {

        const data = new MnistDataset();
        await data.load();

        const batchSize = 50000;
        const rawDataset = data.getAllDataset(batchSize);

        // const images = [];
        const labels = [];

        const zeroes = [];
        const ones = [];
        const twos = [];
        const threes = [];
        const fours = [];
        const fives = [];
        const sixs = [];
        const sevens = [];
        const eights = [];
        const nines = [];

        for(let i = 0; i < batchSize; i++) {
            const imageTensor = tf.tidy(() => {
                // Reshape the image to 28x28 px
                return rawDataset.images
                    .slice([i, 0], [1, rawDataset.images.shape[1]])
                    .reshape([28, 28, 1]);
            });
            const labelTensor = tf.tidy(() => {
                return rawDataset.labels.slice([i, 0], [1, rawDataset.labels.shape[1]]).reshape([1, 10]);
            });

            const canvas = document.createElement('canvas');
            canvas.width = 28;
            canvas.height = 28;
            canvas.style = 'margin: 4px;';
            await tf.browser.toPixels(imageTensor, canvas);


            const label = labelTensor.dataSync().indexOf(Math.max(...labelTensor.dataSync()));
            labels.push(label);

            if(label === 0) {
                if(zeroes.length < number) zeroes.push(canvas);
            }
            if(label === 1) {
                if(ones.length < number)  ones.push(canvas);
            }
            if(label === 2) {
                if(twos.length < number)  twos.push(canvas);
            }
            if(label === 3) {
                if(threes.length < number)  threes.push(canvas);
            }
            if(label === 4) {
                if(fours.length < number)  fours.push(canvas);
            }
            if(label === 5) {
                if(fives.length < number)  fives.push(canvas);
            }
            if(label === 6) {
                if(sixs.length < number)  sixs.push(canvas);
            }
            if(label === 7) {
                if(sevens.length < number)  sevens.push(canvas);
            }
            if(label === 8) {
                if(eights.length < number)  eights.push(canvas);
            }
            if(label === 9) {
                if(nines.length < number)  nines.push(canvas);
            }

            imageTensor.dispose();
            labelTensor.dispose()
        }

        const images = zeroes.concat(ones).concat(twos).concat(threes).concat(fours).concat(fives)
                                .concat(sixs).concat(sevens).concat(eights).concat(nines);
        return images;
        }

    static getSortDataset = async(numGet) => {

    }

}