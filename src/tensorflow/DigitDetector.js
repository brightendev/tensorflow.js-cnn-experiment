import * as tf from '@tensorflow/tfjs';


export class DigitDetector {

    static train = async (datasetCanvas) => {
        const pixels = convertDatasetCanvasToTensorPureJs(datasetCanvas);
        console.log('js', pixels);

        const tensor = convertDatasetCanvasToTensor(datasetCanvas);
        console.log(tensor.dataSync())
    }


}

function convertDatasetCanvasToTensorPureJs(datasetCanvas) {
    const IMAGE_WIDTH = 28;
    const IMAGE_HEIGHT = 28;

    const ctx = datasetCanvas.getContext('2d');
    const pixels = ctx.getImageData(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT).data;
    const singleChannelImagePixels = [];

    // All channels hold an equal value since the image is grayscale, so just read the red channel.
    for(let i = 0; i < pixels.length; i+=4) {
        singleChannelImagePixels.push(pixels[i]);
    }

    const tensor = tf.tensor3d(singleChannelImagePixels, [28, 28, 1], 'int32');
    return singleChannelImagePixels;
}

function convertDatasetCanvasToTensor(datasetCanvas) {

    const IMAGE_WIDTH = 28;
    const IMAGE_HEIGHT = 28;

    const ctx = datasetCanvas.getContext('2d');
    const pixels = ctx.getImageData(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT).data;

    const tensor = tf.browser.fromPixels(new ImageData(pixels, 28, 28), 1);

    return tensor;
}

